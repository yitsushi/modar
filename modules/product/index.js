var Product = new (function Product() {
  var products = [
    { _id: 1, name: "Bike 1", description: "This is the first bike", partnumber: "bk-1b3af249" },
    { _id: 2, name: "Bike 2", description: "This is the second bike", partnumber: "bk-1b3af248" },
    { _id: 3, name: "Bike 3", description: "This is the third bike", partnumber: "bk-1b3af229" },
    { _id: 4, name: "Bike 4", description: "This is the fourth bike", partnumber: "bk-1b3ac249" }
  ];
  this.findById = function findById(id, callback) {
    for(i in products) {
      if (products[i]._id == id) {
        return callback(null, products[i]);
      }
    }

    return callback("Not found", null);
  };
});

module.exports = function application(app) {
  app.get(":slug([a-z0-9\-]+)-p:id([0-9]+).:format", function view(req, res) {
    Product.findById(req.params.id, function(err, product) {
      return res.render(
        "product/view",
        {
          title: product.name + " (" + product.partnumber + ")" + " | Origanum Kft.",
          product: product
        }
      );
    });
  });
};