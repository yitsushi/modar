var UrlRouter   = require('urlrouter');

var RouterProxy = function(app, prefix) {
  this.get = function(path, callback) {
    app.get(prefix + path, callback);
  };
  this.post = function(path, callback) {
    app.post(prefix + path, callback);
  };
};

module.exports = UrlRouter(function Application(app) {
  app.get("/",  function root(req, res) {
    res.end("ready");
  });

  require('./product')(new RouterProxy(app, "/"));
});