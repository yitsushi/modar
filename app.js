var Connect       = require('connect'),
    Configuration = require('./config'),
    connectJade   = require('connect-jade'),
    router        = require('./modules');

var app = Connect()
          .use(Connect.logger('dev'))
          .use(Connect.static('public'))
          .use(Connect.compress())
          .use(Connect.bodyParser())
          .use(Connect.cookieParser(Configuration.Session.secret))
          .use(connectJade({
            root: __dirname + "/views",
            debug: true,
            defaults: {
              title: "No title"
            }
          }))
          .use(router);

var PORT = process.env.PORT || 3000;

app.listen(PORT, function listen() {
  console.log('Application started...');
});
